## Message View

The Message View within Jiggle is used to output various information in text
format.  When an event is first loaded, nothing appears in the Message View.
However, when [HYPOINVERSE is
run](#step-3-recalculate-event-location-and-origin-time-location-view) with the 
"<span style="color:red">Calculate Location</span>"
![bullseye](img/bullseye.gif) tool, the Message view displays the output of the
HYPOINVERSE program.  The Message View also allows users to view the [properties
file contents](../properties/#view-properties-within-jiggle) (notice that the  
Message View ![file](img/file.gif) icon now is highlighted with a blue
background within the "<span style="color:red">Tab views</span>" red box), and
other information accessible through the "<b>Dump</b>" menu in the "<span
style="color:red">Menu bar</span>".

