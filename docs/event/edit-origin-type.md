# Edit Origin Type

Origin type for an event can be modified from **Event > Edit Origin...** menu in Jiggle.

To change the current origin type for an event, navigate to **Event > Edit Origin...** and select the origin type from `Origin Type` drop-down list.
Click **OK** when done.

![Edit Origin](../img/edit-origin.png "Edit Origin Type")

Notes:

- Using an origin type that is not defined in your database will cause database error when saving your event. To make changes to existing origin types, see [Origin Types Configuration](../../configuration/origin-type/ "Origin Types Configuration")
- Relocating an event after changing the origin type may reset your origin type to `Hypocenter`. If origin type is not `Unknown` or `Hypocenter`, Jiggle will confirm your selection before relocating.

