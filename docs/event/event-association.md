# Event Association

When multiple sources report an event, there may be cases where there are multiple events in the Catalog View for the same physical event. 
In such cases, Jiggle allows related events to be associated using `Event Association` context menu.

## Create Association

To create a new event association:

1. In the Catalog View, select an event that should serve as the `Master Event` and right-click to launch the context menu.
1. Navigate to **Event Association... > New Association...**.

    ![New Event Association Menu](../img/event/event_assoc_menu_new.png "New Event Association Menu")

1. On the `Create Event Association` dialog, the `Master Event` should be pre-populated to the event selected on the Catalog View. 
Select associated events from the `Available Events` list and click `Add ->` to move to the `Selected Events` list.
   
    ![Create Event Association](../img/event/event_assoc_create_dialog.png "Create Event Association")

1. Click OK to close the dialog and save the association.
1. Associated events should display in the `ASSOCIATION` column in the Catalog View if the column has been enabled.

    ![Catalog View with ASSOCIATION column](../img/event/event_assoc_catalog_view.png "Catalog View with ASSOCIATION column")

## Edit Association

To modify an existing event association:

1. In the Catalog View, select the event to modify and right-click to launch the context menu.
1. Navigate to **Event Association... > Edit Association...**.

    ![Edit Event Association Menu](../img/event/event_assoc_menu_edit.png "Edit Event Association Menu")

1. `Edit Event Association` dialog should be pre-populated with the selected event as the `Master Event` and 
previously associated events added in `Selected Events` list.
   
    ![Edit Event Association](../img/event/event_assoc_edit_dialog.png "Edit Event Association")

1. Select an event from the `Available Events` or `Selected Events` and click `Add ->` or `<- Remove` to update the association.
1. Click OK to close the dialog and save the association.

## Delete Association

To delete an existing event association:

1. In the Catalog View, select the event to modify and right-click to launch the context menu.
1. Navigate to **Event Association... > Delete Association...**.

    ![Delete Event Association Menu](../img/event/event_assoc_menu_delete.png "Delete Event Association Menu")

1. Click OK on the `Delete Event Association` confirmation dialog. 

    ![Delete Event Association](../img/event/event_assoc_delete_dialog.png "Delete Event Association")
   
## Create Association by Selecting Multiple Events

When a list of events to be associated are known from the Catalog View, event association may be created by selecting multiple events from the Catalog View and selecting **Event Association... > New Association...**
 from the context menu.

Notes: Multiple events must be selected from the **ID** column in the Catalog View.

1. Select two or more events that do not have any current associations. Select **Event Association... > New Association...**.
1. On the Create Event Association dialog, both the `Master Event` and `Selected Events` should be populated with all the events selected from the catalog view. 

    ![Create Event Association with Multiple Events](../img/event/event_assoc_create_multiselect_dialog.png "Create Event Association with Multiple Events")
   
1. Choose a master event in the `Master Event` list. `Selected Events` should be automatically adjusted by Jiggle to remove the selected master event from the list.
1. Click OK to save the association.

## Catalog View - Association Column

`ASSOCIATION` column may be added to the Catalog View to see a list of associated events on an event row in the view.

To add the `ASSOCIATION` column:

1. Navigate to **Properties > Edit Properties...** using the top menu.
1. Select **Catalog Columns** tab.
1. Select `ASSOCIATION` from the `Available to add` and click on right arrow to add to `Catalog table`.
1. Click OK to save the changes. 


## Optional Configuration

By default, the `Available Events` list shows all events that are displayed in the Catalog View. 
If the available events list is too long, there is an option to only display those events that are within the user defined time from the master event.

To update the configuration:

1. From the Catalog View, click on the ![Filter Catalog View](../img/filter_blue.gif "Filter Catalog View") menu icon for `Define Event Selection Criteria`.
1. Click **Edit** button to display the `Event Selection` dialog.
1. On the `Time Range` tab, change the selection under `Events displayed on the Event Association Dialog`.
   
    - `All Events from Catalog View` - display all events from the catalog view.
    - `Only events from Catalog View that are within the set time from the master event` - display events that are within +/- user defined minutes from the master event. 
   
        ![Event Association Configuration Options](../img/event/event_assoc_time_range.png "Event Association Configuration Options")
   
1. Click OK to save the changes. 

