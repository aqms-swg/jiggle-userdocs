# Setting Event Details

## Set an Event as Regional Earthquake with Comment

Event's detail can be edited using Jiggle's toolbar menu or **Event** menu, but
**Event > Set Event Type > Earthquake - Regional** menu provides a shortcut to 
set an event as a Regional Earthquake with a default comment. This dialog is intended to quickly 
mark an event as a Regional Earthquake, but can be used to set other event type, geographic type, and comments as well.

To launch the dialog, navigate to
**Event > Set Event Type > Earthquake - Regional**. You can also click **Ctrl-R** or **Command-R** on macOS to launch the dialog 
when an event has been loaded. 

![Regional - Earthquake](../img/set-regional-earthquake.png "Regional - Earthquake")

When the dialog opens, it has the following defaults:

- Event Type - Earthquake
- Geographic Type - Regional
- Comment - blank or empty string by default. Configurable from Preference dialog **Properties > Edit Properties > Misc**.

Notes:

- Setting an event as `Quarry` may cause another dialog to popup that asks for comments. 
This is intentional so that the default quarry comment may be used. 
- See [comment.earthquake.regional](../../properties/) for configuration setting for default comments.
