# About Jiggle

Jiggle is a Graphical User Interface (GUI) software application used to analyze
earthquake waveform data and calculate accurate earthquake (event) parameters.

Jiggle is part of the post-processing (PP) software suite in the ANSS Quake
Monitoring System (AQMS).  ANSS = Advanced National Seismic System.

Human analysts at different Regional Seismic Networks (RSN) use Jiggle to come
up with a high-quality earthquake catalog. These catalogs are essential for
monitoring earthquake hazards in the region and form the basis for research
efforts in earthquake science.

Immediately following an earthquake, real-time (RT) software in AQMS will
automatically determine arrival times of seismic waves at different stations
across the region and solve for its approximate origin and magnitude.  An
earthquake's origin is defined as its location (latitude, longitude, depth) and
origin time, i.e. the time when the earthquake started, in
Coordinated Universal Time [UTC]. The RT software may make mistakes in
identifying earthquakes, and the resulting earthquake parameters are not
accurate enough.  Analysts use the Jiggle GUI to visually review
event waveforms (time series of ground motion) at seismic stations, manually
pick the precise arrival time of P and S seismic waves, and update the event's
origin and magnitude.

Jiggle is used at many ANSS RSNs, such as: SCSN, AVO, CERI, HVO, NCSN, NEIC,
MBMG, PNSN, UUSS, LCSN. 

# About the Jiggle User documentation
This documentation is split into two main parts. The Installation and Configuration 
section has detailed information on how to configure Jiggle and describes some 
of the algorithms it applies. The Using Jiggle section has a detailed description 
and examples of the GUI itself. To navigate to these main sections, use the top 
navigation panel. One can navigate sequentially through the pages via the Next 
and Previous buttons, or jump to different sections using the side bar.

The Using Jiggle section has some content (screenshots, analyst practices)
that is specific to the setup at the [Southern California Seismic Network
(SCSN)](http://www.scsn.org), located at Caltech in Pasadena, California.
